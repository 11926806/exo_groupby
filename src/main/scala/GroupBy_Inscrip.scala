import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import org.slf4j.LoggerFactory
import akka.stream.scaladsl.MergeHub
import akka.stream.scaladsl.Sink
import akka.stream.scaladsl.Keep
import scala.concurrent.duration._
import scala.language.postfixOps
import akka.stream.scaladsl.Source
import akka.stream.scaladsl.BroadcastHub
import akka.stream.scaladsl.Flow
import java.nio.file.Paths
import scala.concurrent.Future
import akka.stream.scaladsl.FileIO
import akka.stream.IOResult
import akka.stream.alpakka.csv.scaladsl.CsvParsing
import akka.stream.KillSwitches

object GroupBy_Inscrip extends App {
  implicit val system = ActorSystem(Behaviors.empty, "system")
  import system.executionContext
  val log = LoggerFactory.getLogger("monLogger")

  //structures
  case class Inscription(id: Int, annee: Int, formation: String)
  case class Parcours(nb:Int,trace:String)

  //les inscriptions
  val inscriptions_list = List(Inscription(1,2018,"L1"),
    Inscription(1,2019,"L2"),
    Inscription(2,2020,"L3"),
    Inscription(3,2019,"L2"),
    Inscription(4,2018,"L1"),
    Inscription(4,2020,"L2"),
    Inscription(4,2020,"L3"),
    Inscription(1,2020,"L3"))


  //flux d'inscriptions à partir d'une liste
  val Inscriptions = Source(inscriptions_list)
     Inscriptions
    .groupBy(Int.MaxValue,(I)=>I.id)
    .groupBy(Int.MaxValue,(I)=>I.formation)
    .map((I)=>I.formation)
    .mergeSubstreams
    .reduce((acc,element)=>acc+"-"+element)
    .map((s)=>Parcours(1,s))
    .mergeSubstreams
    .groupBy(Int.MaxValue,(P)=>P.trace)
    .reduce((P1,P2)=>Parcours(P1.nb+P2.nb,P1.trace))
    .mergeSubstreams
    .to(Sink.foreach(println)).run()



  val file = Paths.get("example.csv")

  //lecture à partir du fichier
     FileIO.fromPath(file)
    .via(CsvParsing.lineScanner())
    .map(_.map(_.utf8String).toVector)
    .map((xs) => Inscription(xs.head.toInt, xs(1).toInt, xs.last))
    .groupBy(Int.MaxValue,(I)=>I.id)
    .groupBy(Int.MaxValue,(I)=>I.formation)
    .map((I)=>I.formation)
    .mergeSubstreams
    .reduce((acc,element)=>acc+"-"+element)
    .map((s)=>Parcours(1,s))
    .mergeSubstreams
    .groupBy(Int.MaxValue,(P)=>P.trace)
    .reduce((P1,P2)=>Parcours(P1.nb+P2.nb,P1.trace))
    .mergeSubstreams
    .to(Sink.foreach(println)).run()



  Thread.sleep(30000)
  log.info("Sayonara")
  system.terminate()
}

